# quickstart-scala-spark-gradle #

An opinionated quickstart of how to use scala with gradle. (Fork me!)

By default, this will create a project containing a great base to make a production-level Spark application.

The intention is to fork this repo (or clone & copy), keeping only what you need.

# Includes... #

- `Scala 2.11` for **coding**
- `Gradle 4` for **building**
- `Spark 2.3.0` for **processing**
- `application` gradle plugin for **running in-place** _(default runner via `./gradlew run`)_
- `shadowjar` gradle plugin for **distributing & running as a package** _(builds a fat jar with all dependencies via `./gradlew shadowjar`, the jar can then be run by YARN/Mesos)_
- `idea` gradle plugin for **developing** _(Develop [as a gradle project](https://www.jetbrains.com/help/idea/import-project-or-module-wizard.html) using [IntelliJ](https://www.jetbrains.com/idea/))_
- `scalatest` for **testing** _(default test runner via `./gradlew test`)_
- `scoverage` for **test coverage** _(statement-level coverage via `./gradlew scoverage`)_
- `scalalogging -> slf4j -> log4j2` for **logging** _(scala code -> interface -> mechanism)_
- `scopt` for command-line **argument parsing**
- `pureconf -> typesafe config` for **configuration parsing** _(scala wrapper -> mechanism)_
- `scaladoc` for **documentation** _(via `./gradlew scaladoc`)_

Check out the [Libs](#markdown-header-links) below for more details on each.

# How to... #

## Build: ##

Compile, test, and create a fat jar:
```sh
./gradlew build # jar created under build/libs/.
```

## Test: ##

Test the code using scalatest:
```sh
./gradlew test # In-depth readable reports will be generated in build/reports/tests
./gradlew scoverage reportscoverage  # In-depth readable reports will be generated in build/reports/scoverage
```

## Clean: ##

Completely clean this project's build (but not the gradle cache):
```sh
./gradlew clean
```

## Run: ##

If you want to run **while the repo is present...**
```sh
./gradlew run
```

---

If you want to run **in isolation...**

1. Build the jar once: `./gradlew build`
3. Distribute to your heart's content (example shown): `scp /build/libs/quickstart-scala-spark-gradle-*.jar user@wherever:.`
2. Have your clients run the jar (only requires the jar, no code or repo): `java -jar quickstart-scala-spark-gradle-*.jar`

A quick sanity check can be done via the following:
```sh
./gradlew build
java -jar build/libs/quickstart-scala-spark-gradle-*.jar
```


# Libs #

Below are the libraries used to provide a broad starting base for quickstart-scala-spark-gradle.


## [spark](https://spark.apache.org/) - big data processing framework ##

You should really know what `spark` is if you're looking to use this repo.

###### Why do we use it? ######

- Use lots of machines easily.
- Process large or complex volumes of data at scale.
- Simplify data flows.
- To hog all the memory.

###### How do we use it? ######

- **Locally:** Running `./gradlew run`
- **On a cluster:** Create a fat jar with `./gradlew shadowjar` or `./gradlew shadowjar` and [submit the jar to the cluster with `spark-submit`](https://spark.apache.org/docs/latest/submitting-applications.html).




## [shadowjar](https://github.com/johnrengelman/shadow) - bundled jars ##

`shadowjar` is a gradle plugin that allows us to build a large jar that is packed with all of its dependencies.

This is used for spark to create a jar that is runnable and can be [submit with `spark-submit`](https://spark.apache.org/docs/latest/submitting-applications.html) (for when you bring your application to the cluster).

Shadowjar is analogous to the Maven "shade" plugin.

###### Why do we use it? ######

- Build jars that can run on their own
- Build jars that can be run via `spark-submit`
- Build jars that hide conflicting dependencies

###### How do we use it? ######

- By running `./gradlew shadowjar` to generate a jar
- Integrated with `./gradlew build` to do the whole build process, generating a jar



## [scalatest](https://github.com/scalatest/scalatest) - test framework ##

`scalatest` is a testing framework (like JUnit/RSpec/Chai/Mocha).

###### Why do we use it? ######

- JUnit just doesn't cut it for scala.
    - JUnit assertions & matchers can be simplified.
    
    - JUnit supports only test specifications.
    
- Scalatest gives many options when it comes to style.
    - Specification style (like Rspec).
    
    - Behavior Specifications (like cucumber).
    
    - Basic build-your-own-spec.
    
    - Even JUnit style.
    
- The matchers and assertions are amazing (try to `assert(something)`, it gives great results.)

###### How do we use it? ######

- All tests run every time the program is built (`./gradlew build`), providing that run-time guarantee that everything is working.
- Gradle plugin [gradle-scalatest](https://github.com/maiflai/gradle-scalatest) allows us to run the scalatest framework as the default testrunner for gradle via `./gradlew test`.
- HTML Reports are generated on every test run, and are dumped in `build/reports/`
- All tests in `src/test/...` are written using scalatest.



## [scoverage](https://github.com/scoverage) - coverage ##

`scoverage` is a scala plugin that works in conjunction with scalatest that allows the generation of "coverage reports".  Coverage reports allow us to see what portions of our codebase have been hit by tests.  Most importantly, scoverage generates reports at a statement-level, so things like anonymous functions are checked for usage correctly.

###### Why do we use it? ######

- Coverage reports let you know where you should be focusing on testing.
- Coverage reports allow you to reason about technical debt and liability in your codebase.
- Statement-level coverage (rather than line-level) is important to scala because of the abundance of complex statements (blocks, anonymous functions, macros, etc...)

###### How do we use it? ######

- Gradle plugin [gradle-scoverage](https://github.com/scoverage/gradle-scoverage) allows us to run scoverage on top of the gradle test task. Due to scalatest being the default test runner, this will run scoverage with scalatest via `./gradlew scoverage` or `./gradlew reportscoverage`
- HTML and XML Reports are generated via `./gradlew reportscoverage`, and are dumped in `build/reports/`
- scoverage is not integrated into any default gradle tasks, and as such will not run on `./gradlew test` or `./gradlew build`



## [scopt](https://github.com/scopt/scopt) - argument parsing ##

`scopt` is a framework that allows succinct and powerful command-line argument parsing.

###### Why do we use it? ######

- Parsing arguments is a chore that has been solved.
- Self-documenting with standardization.
- Makes it easy to simultaneously standardize, document, validate, and parse.
- Options like `Argot` are deprecated, where others like `Scallop` allow arguments that get confusing to document (passing `-42` where the argument is numerically flexible).

###### How do we use it? ######

- Refer to the entry point of the application to see it in action.

###### Quick Example: ######

`src/main/scala/com/example/Main.scala`:
```scala
import scopt.OptionParser

object Main {

  final case class OurArguments(maybeName: Option[String] = None)

  object ArgParser extends OptionParser[OurArguments]("project-name") {

    head("program description goes here")

    help("help").text("prints this usage text")

    opt[String]('n', "name")
      .text("Add a description for the name argument")
      .optional() // signifies this is not a required argument
      .validate( name => if (name == "sam") failure("name can't be 'sam'") else success )
      .action( (newName, conf) => conf.copy(maybeName = Some(newName)) )
  }

  def main(args: Array[String]) {
    val defaultArguments = OurArguments()

    val maybeParsedArgs: Option[OurArguments] = ArgParser.parse(args, defaultArguments)

    maybeParsedArgs match {
      case Some(parsedArgs) =>
        // Go nuts
      case None =>
        ArgParser.showUsageAsError()
        // Exit, log it, report it, whatever.
    }
  }
}
```


## [pureconfig](https://github.com/pureconfig/pureconfig) - configuration parsing ##

`pureconfig` is a framework that allows succinct and powerful configuration parsing & management.  It is designed to target scala, and uses many scala idioms (you'll find yourself slowly being led to write monoids for your configurations and using `pureconfig` to represent your configuration as a data structure.)

###### Why do we use it? ######

- Parsing configurations is a chore that has been solved.
- It allows us to keep our configuration DRY and commented (refer to `src/main/resources/test.conf`)
- Allows multiple configuration types (`.properties`, `.conf`, `.json`).
- It doesn't force us to make decisions about how we use or store configuration files or streams, it just does the heavy lifting (while providing sensible defaults like `test.conf`).
- Options like `JCommander` and plain `typesafe config` are too java-specific, while `sbt` configurations are too simple. `pureconfig` is just right.

###### How do we use it? ######

- `pureconfig` uses [typesafe config](https://github.com/typesafehub/config) to handle configuration files. `pureconfig` is really just a pretty scala interoperability layer.  Expect to be using and passing around instances of the `com.typesafe.config.Config` class.
- Refer to the entry point of the application to see it in action.

###### Quick Example: ######

`src/main/resources/application.conf`:
```conf
// Notice these two are not used in the configuration object
firstName = "Mr."
lastName = "Wooster"
// And that we can express concatenation here in the configuration
name = ${firstName} ${lastName}
age = 30
```

`src/main/scala/com/example/Main.scala`:
```scala
import com.typesafe.config.{Config, ConfigFactory}

object Main {

  final case class OurConfiguration(name: String, age: Int)

  def main(args: Array[String]) {

    // Use typesafe config to load config with sensible defaults
    val defaultConfig: Config =
      ConfigFactory           // Typesafe ConfigFactory for loading Configs
        .defaultApplication() // Defaults to this classloader's resources' `appliction.conf`
        .resolve()            // Not necessary unless your config has any imports, var substitutions, conditionals, etc...

    // Use pureconfig to convert to our scala object
    val configuration: OurConfiguration = pureconfig.loadConfigOrThrow[OurConfiguration](defaultConfig)

    // Go nuts
  }
}
```


## [scala-logging](https://github.com/typesafehub/scala-logging) - universal logging ##

`scala-logging` is a logging adapter specific to Scala that uses the [slf4j logging interface](https://www.slf4j.org/manual.html).  In turn, that interface is hooked up to [Log4J as the logging mechanism](http://logging.apache.org/log4j/2.x/manual/layouts.html) (the thing that actually logs), which has been sensibly preconfigured.

###### Why do we use it? ######

- `scala-logging` gives you a limited subset of the functionality (only the stuff you need).
- `scala-logging` is coupled tightly to scala, and thus is very performant, making use of scala macros to intelligently not log when the logging level is not requested.  This allows us to leave trace & debug logs everywhere we need them without worrying about performance.
- `Log4J2` can be drop-in-replaced.  If you want to use logback (or whatever logging mechanism), find the logback-slf4j-adapter, and configure logback.

###### How do we use it? ######

- Modify `src/main/resource/log4j2.xml` to your desire. It currently has good defaults for moderate log usage in production (20 files rotating, 25 MB max each, INFO level)
- Inherit `LazyLogger` to a class and use the logger (or don't).
- Alternatively, instantiate a new Logger and pass it a name or a class.

###### Quick Example: ######

`src/main/scala/com/example/Main.scala`:
```scala
import com.typesafe.scalalogging.LazyLogging

object Main extends LazyLogging {
  def main(args: Array[String]) {
    logger.info("Hello, World!")
  }
}
```


## Who do I talk to? ##

- Email: chap.s.b@gmail.com
- Bitbucket: https://bitbucket.org/sbchapin/
- Discord: sbchapin#7435

## What's the future of this repo? ##

- Keep this baby up to date
- [Report issues & provide suggestions](https://bitbucket.org/sbchapin/quickstart-scala-spark-gradle/issues/new)
- More robust examples of the existing libraries
- CI & CD Integrations
