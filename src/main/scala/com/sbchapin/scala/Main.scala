package com.sbchapin.scala

import com.sbchapin.scala.args._
import com.sbchapin.scala.config._
import com.typesafe.scalalogging.LazyLogging


/**
  * Examples of:
  * - Logging
  * - Argument parsing
  *   - Documentation
  *   - Validation
  *   - Object generation
  * - Configuration parsing
  *   - Local variables
  *   - Interpolation
  *   - Object structure
  * - Basic "Job" structure
  *   - One job per execution
  *   - Backed by configuration
  *   - Mirrored in code (Job, Job1, Job2, JobArguments, Job1Arguments, Job2Arguments)
  *   - Ready for dependency injection with module structure (JobSupplier, Job1Module, Job2Module, SparkModule)
  *
  * @author sbchapin
  * @since 10/3/17.
  */
object Main extends LazyLogging {

  /**
    * How to parse args for main entrypoint.
    */
  class ArgParser extends scopt.OptionParser[MainArguments]("quickstart-scala-spark-gradle-pipeline") with LazyLogging {

    val defaultArgs = MainArguments()

    def parseOrDie(args: Array[String]): MainArguments = {
      val parsedArgs = parse(args, defaultArgs).getOrElse(defaultArgs)

      // Enforces presence of a command
      if (parsedArgs.jobArgs.isEmpty) {
        logger.error(this.usage)
        throw new RuntimeException(s"Must pass a command as an argument.  Possible commands: ${this.commands.map(_.name).mkString(", ")}")
      }
      parsedArgs
    }

    head("quickstart-scala-spark-gradle", "You'll be wanting to set a decent description for the help text of your program here")

    help("help").text("Print this usage text")

    opt[Unit]("prod")
      .optional()
      .text("Pass this ONLY if running in production. It will use the production.conf instead of test.conf and will create and use production data.")
      .action((_, existingArgs) => existingArgs.copy(isProd = true))

    cmd("job-1")
      .action((_, args) => args.copy(jobArgs = Some(Job1Arguments(
        // default job configuration:
        size = 5
      ))))
      .text("Run Job 1.")
      .children(
        opt[Int]("size")
          .valueName("<size>")
          .optional()
          .text("The size for job 1.")
          .validate { size => if (size > 0) success else failure("must be greater than 0") }
          .action { case (newSize, existingArgs) =>
            // Replace inner object with a copy of itself (after updating it)
            existingArgs.copy(jobArgs = existingArgs.jobArgs.map{
              case existingJobArgs: Job1Arguments => existingJobArgs.copy(size = newSize)
              case _ => throw MalformedCommandException
            } )
          }
      )

    cmd("job-2")
      .action((_, args) => args.copy(jobArgs = Some(Job2Arguments(
        // default job configuration
        coolWords = Seq("phantasm", "quotidian", "brobdingnagian")
      ))))
      .text("Run Job 2.")
      .children(
        opt[Seq[String]]("cool-words")
          .valueName("<word1>,<word2>,...,<wordN>")
          .optional()
          .text("Cool words to use in Job 2.")
          .validate { words => if (words.isEmpty) failure("must not be empty") else success }
          .action { case (newCoolWords, existingArgs) =>
            // Replace inner object with a copy of itself (after updating it)
            existingArgs.copy(jobArgs = existingArgs.jobArgs.map{
              case existingJobArgs: Job2Arguments => existingJobArgs.copy(coolWords = newCoolWords)
              case _ => throw MalformedCommandException
            } )
          }
      )

  }

  /**
    * Entrypoint for data pipeline.  This can be either run as a jar with local spark, or as a jar distributed to a spark cluster.
    *
    * Don't be afraid to run it locally without flags.
    *
    * If this is your first time seeing this code, run it with `--help` to see a cat-like explanation of your options.
    *
    * @param commandLineArgs arguments to tell the jar what to do.  Expects either a command, or `--help`.
    */
  def main(commandLineArgs: Array[String]): Unit = {
    // Arg Parse
    logger.debug(s"Attempting to parse passed arguments: ${commandLineArgs.mkString(" ")}")
    val args: MainArguments = new ArgParser().parseOrDie(commandLineArgs)
    logger.debug(s"Successfully parsed arguments: $args")

    // Config Parse
    logger.debug(s"Attempting to parse configuration: ${args.configurationLocation}")
    val config: MainConfiguration = pureconfig.loadConfigOrThrow[MainConfiguration](args.conf)
    logger.debug(s"Successfully loaded configuration: $config")

    // Start the controller
    val controller = new MainController(args, config)
    logger.debug(s"Handing control over to $controller")
    controller.start()

  }
}
