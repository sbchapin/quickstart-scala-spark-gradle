package com.sbchapin.scala

import com.typesafe.config.{Config, ConfigFactory, ConfigParseOptions, ConfigSyntax}

package object args {

  case class MainArguments(isProd: Boolean = false,
                           jobArgs: Option[JobArguments] = None) {
    def configurationLocation: String = if (isProd) "prod.conf" else "test.conf"
    def conf: Config = ConfigFactory.parseResources(configurationLocation, ConfigParseOptions.defaults().setSyntax(ConfigSyntax.CONF)).resolve()
    def sparkMode: SparkModes.SparkMode = if (isProd) SparkModes.SparkSubmit else SparkModes.Local
  }

  sealed trait JobArguments
  case class Job1Arguments(size: Int) extends JobArguments
  case class Job2Arguments(coolWords: Seq[String]) extends JobArguments


}
