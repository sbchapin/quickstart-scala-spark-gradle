package com.sbchapin.scala.args

/**
  * @author sbchapin
  * @since 7/3/18.
  */
object MalformedCommandException extends RuntimeException("Guaranteed by command.action(...) to be populated with mapped argument type.  This should be unreachable - check that you have mapped the correct type and have provided the correct type to the .action(...) call of the wrapping command within the arg parser.")
