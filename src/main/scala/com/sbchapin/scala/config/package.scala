package com.sbchapin.scala

package object config {

  /**
    * Data type to represent configuration that is supplied to this program - compiletime, runtime, or otherwise.
    *
    * @param job1 Basic configuration for IO of job 1
    * @param job2 Basic configuration for IO of job 2
    */
  case class MainConfiguration(job1: TransformJobConfiguration, job2: TransformJobConfiguration)

  /**
    * A very basic spark job that reads in and writes out
    * @param in  Path of inputs
    * @param out Path of outputs
    */
  case class TransformJobConfiguration(in: String, out: String)


}
