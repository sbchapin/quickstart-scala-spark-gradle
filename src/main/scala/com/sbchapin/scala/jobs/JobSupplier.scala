package com.sbchapin.scala.jobs

/**
  * Supplies a job when called upon.
  * @author sbchapin
  * @since 7/3/18.
  */
trait JobSupplier {
  def job: Job
}
