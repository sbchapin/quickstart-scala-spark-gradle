package com.sbchapin.scala.jobs

/**
  * Top-level runnable "Job":
  *
  * Each job should represent a multi-stage spark job (usually I/O).
  * You should be thinking about each Job as a command submitted by an execution of this program.
  * Note that the wiring & configuration of the job is deferred to a `JobSupplier` - there is inherent parity worth understanding.
  *
  *
  * Configuration:
  *
  * The configuration for a job is managed by its `JobSupplier`.
  * Each job in this example has been created with a module JobSupplier (`Job1 extends Job`, `Job1Module extends JobSupplier`).
  * This pattern will help you when the project grows in scope and you desire to start dependency-injecting.  As of right now it simply keeps the dependencies clear via a slim Cake Pattern. (angel food cake?)
  *
  * @author sbchapin
  * @since 6/12/18.
  */
trait Job {
  def run(): Unit
}