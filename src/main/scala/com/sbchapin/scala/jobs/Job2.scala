package com.sbchapin.scala.jobs

import org.apache.spark.sql.SparkSession

/**
  * See `Job` documentation as a reference for (`Job1`, `Job1Module`) pattern.
  * Note that this is different from job1 in that it uses registries for its readers and writers.
  *
  * This simple job reads in parquet, filters by only the cool words, and finally writes out parquet.
  *
  * @param inPath    Where to read from.
  * @param outPath   Where to write to.
  * @param coolWords Which words to allow.
  * @author sbchapin
  * @since 7/2/18.
  */
case class Job2(inPath: String, outPath: String, coolWords: Seq[String])(implicit spark: SparkSession) extends Job with Serializable {

  import spark.implicits._

  override def run(): Unit = {
    // Read
    val in = spark.read.parquet(inPath).as[String]
    // Process
    val out = in.filter(coolWords.contains _)
    // Write
    out.write.parquet(outPath)
  }
}

/**
  * Mixable and Dependency-Injectable "Module" containing the wiring for a job and a supplier (lazy val) for a job.
  */
trait Job2Module extends JobSupplier {
  // Spark
  val spark: SparkSession

  // I/O Paths
  val inPath:  String
  val outPath: String

  // Job parameters
  val coolWords: Seq[String]

  // Job
  override lazy val job: Job2 = new Job2(inPath, outPath, coolWords)(spark)
}