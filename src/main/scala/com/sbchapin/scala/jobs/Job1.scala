package com.sbchapin.scala.jobs

import org.apache.spark.sql.SparkSession

/**
  * See `Job` documentation as a reference for (`Job1`, `Job1Module`) pattern.
  *
  * This simple job reads in parquet, limits to configured size, and finally writes out parquet.
  *
  * @param inPath  Where to read from.
  * @param outPath Where to write to.
  * @param size    How many records to limit to.
  * @author sbchapin
  * @since 7/2/18.
  */
case class Job1(inPath: String, outPath: String, size: Int)(implicit spark: SparkSession) extends Job {
  override def run(): Unit = spark.read.parquet(inPath).limit(size).write.parquet(outPath)
}

/**
  * Mixable and Dependency-Injectable "Module" containing the wiring for a job and a supplier (lazy val) for a job.
  */
trait Job1Module extends JobSupplier {
  // Spark
  val spark: SparkSession

  // I/O Paths
  val inPath:  String
  val outPath: String

  // Job parameters
  val size: Int

  // Job
  override lazy val job: Job1 = new Job1(inPath, outPath, size)(spark)
}