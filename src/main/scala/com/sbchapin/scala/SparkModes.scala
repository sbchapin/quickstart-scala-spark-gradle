package com.sbchapin.scala

import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.auth.{AWSCredentialsProviderChain, EC2ContainerCredentialsProviderWrapper, EnvironmentVariableCredentialsProvider, SystemPropertiesCredentialsProvider}
import org.apache.spark.sql.SparkSession

/**
  * Different configurations for running spark.
  *
  * @author sbchapin
  * @since 7/2/18.
  */
object SparkModes {

  sealed trait SparkMode {
    def initSpark(appName: String): SparkSession
  }

  /**
    * Used for test and sample jobs.
    */
  object Test extends SparkMode {

    override def initSpark(appName: String): SparkSession = {

      // Spark initialization
      val sparkSession = {
        SparkSession
          .builder()
          .appName(appName)
          .master("local")
          .config("spark.sql.shuffle.partitions", 5) // assumes you are working with small amounts of data
          .config("spark.ui.enabled", value = false)
          .getOrCreate()
      }

      sparkSession
    }
  }

  /**
    * Used for development and small jobs.
    */
  object Local extends SparkMode {

    override def initSpark(appName: String): SparkSession = {

      // Spark initialization
      val sparkSession = {
        SparkSession
          .builder()
          .appName(appName)
          .master("local")
          .config("spark.sql.shuffle.partitions", 5) // assumes you are working with small amounts of data
          .getOrCreate()
      }

      // AWS integration
      val credentialsProvider = new AWSCredentialsProviderChain(
        new EnvironmentVariableCredentialsProvider,
        new SystemPropertiesCredentialsProvider,
        new ProfileCredentialsProvider,
        new EC2ContainerCredentialsProviderWrapper
      )
      val credentials = credentialsProvider.getCredentials
      sparkSession.sparkContext.hadoopConfiguration.set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
      sparkSession.sparkContext.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", credentials.getAWSAccessKeyId)
      sparkSession.sparkContext.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey", credentials.getAWSSecretKey)
      sparkSession.sparkContext.hadoopConfiguration.set("fs.s3.awsAccessKeyId", credentials.getAWSAccessKeyId)
      sparkSession.sparkContext.hadoopConfiguration.set("fs.s3.awsSecretAccessKey", credentials.getAWSSecretKey)
      sparkSession
    }
  }

  /**
    * Used for spark-submit runs of jobs on clusters.
    */
  object SparkSubmit extends SparkMode {

    override def initSpark(appName: String): SparkSession = {

      // Spark initialization
      val sparkSession = SparkSession
        .builder()
        .appName(appName)
        .getOrCreate()

      // Hadoop conf
      sparkSession.sparkContext.hadoopConfiguration.setInt("fs.s3a.connection.maximum", 5000)
      sparkSession.sparkContext.hadoopConfiguration.set("mapreduce.fileoutputcommitter.algorithm.version", "2")
      sparkSession
    }
  }

}
