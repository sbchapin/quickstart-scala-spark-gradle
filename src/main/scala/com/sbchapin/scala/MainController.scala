package com.sbchapin.scala

import com.sbchapin.scala.args._
import com.sbchapin.scala.config.MainConfiguration
import com.sbchapin.scala.jobs.{Job, Job1Module, Job2Module, JobSupplier}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.SparkSession

/**
  * A multiplexer that decides what jobs to run and how to run them.
  * Arguments that are passed in are first de-serialized within the entrypoint of this program, `Main`.
  *
  * @author sbchapin
  * @since 6/12/18.
  */
class MainController(arguments: MainArguments, configuration: MainConfiguration) extends LazyLogging {

  /**
    * The provider of spark and spark-context related content.
    * Mixes well with other modules.
    */
  trait SparkModule {
    lazy val spark: SparkSession = arguments.sparkMode.initSpark("quickstart-spark-scala-gradle")
  }

  // Wire the modules (designed with modules to accommodate dependency injection for larger projects)
  lazy val job1Supplier: JobSupplier = new Job1Module with SparkModule {
    override val inPath: String = configuration.job1.in
    override val outPath: String = configuration.job1.out
    override val size: Int = arguments.jobArgs.get match {
      case j: Job1Arguments => j.size
      case _ => throw MalformedCommandException
    }
  }
  lazy val job2Supplier: JobSupplier = new Job2Module with SparkModule {
    override val inPath: String = configuration.job2.in
    override val outPath: String = configuration.job2.out
    override val coolWords: Seq[String] = arguments.jobArgs.get match {
      case j: Job2Arguments => j.coolWords
      case _ => throw MalformedCommandException
    }
  }

  /**
    * Starts the process under purview of the pipeline controller.
    */
  def start(): Job = {

    // Multiplex args into correct job
    val job: Job = arguments.jobArgs match {
      case Some(_: Job1Arguments) => job1Supplier.job
      case Some(_: Job2Arguments) => job2Supplier.job
      case None =>                   throw MalformedCommandException
    }

    // Run the job
    job.run()
    job
  }

}
