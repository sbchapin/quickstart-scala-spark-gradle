package com.sbchapin.scala.jobs

import java.io.File

import com.sbchapin.scala.SparkModes
import org.apache.commons.io.FileUtils
import org.apache.spark.sql.{Row, SparkSession}
import org.scalatest.tagobjects.Slow
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * "Integration test" for Job 2.
  *
  * Examples of...
  * - Spark I/O testing
  * - Before & After
  * - "Slow" tag for organizing tests
  *
  * Problems with this test (which maybe you're okay with):
  *
  * - Writes to files that must be read & cleaned up within this test.
  * - Reads output using the same mechanism that is likely writing it.
  *
  * These problems can easily be addressed by extracting the read/write layer. Consider doing this for ambitious spark projects, or at least create a generalized test harness.
  *
  * @author sbchapin
  * @since 7/3/18.
  */
class Job2Spec extends FlatSpec with BeforeAndAfter with Matchers {

  val name: String = this.getClass.getSimpleName
  val tmpDir: String = s"./tmp/test/$name"
  val inDir = s"$tmpDir/in"
  val outDir = s"$tmpDir/out"
  implicit val spark: SparkSession = SparkModes.Test.initSpark(name)

  import spark.implicits._

  before {
    // Create dataset of alphabet before
    spark.createDataset(('a' to 'z').map(_.toString)).write.parquet(inDir)
  }

  after {
    // Cleanup
    FileUtils.deleteDirectory(new File(tmpDir))
  }


  "Job2" should "read parquet from inPath, limit to just the vowels, and write to outPath in parquet" taggedAs Slow in {
    Job2(inDir, outDir, "aeiou".toSeq.map(_.toString)).run()

    val results = spark.read.parquet(outDir).collect()
    results should contain only (
      Row("a"),
      Row("e"),
      Row("i"),
      Row("o"),
      Row("u")
    )
  }

}
