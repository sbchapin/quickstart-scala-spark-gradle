package com.sbchapin.scala.jobs

import java.io.File

import com.sbchapin.scala.SparkModes
import org.apache.commons.io.FileUtils
import org.apache.spark.sql.{Row, SparkSession}
import org.scalatest.tagobjects.Slow
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * "Integration test" for Job 1.
  *
  * Examples of...
  * - Spark I/O testing
  * - Before & After
  * - "Slow" tag for organizing tests
  *
  * Problems with this test (which maybe you're okay with):
  *
  * - Writes to files that must be read & cleaned up within this test.
  * - Reads output using the same mechanism that is likely writing it.
  *
  * These problems can easily be addressed by extracting the read/write layer. Consider doing this for ambitious spark projects, or at least create a generalized test harness.
  *
  *
  * @author sbchapin
  * @since 7/3/18.
  */
class Job1Spec extends FlatSpec with Matchers with BeforeAndAfter {


  val name: String = this.getClass.getSimpleName
  val tmpDir: String = s"./tmp/test/$name"
  val inDir = s"$tmpDir/in"
  val outDir = s"$tmpDir/out"
  implicit val spark: SparkSession = SparkModes.Test.initSpark(name)
  import spark.implicits._

  before {
    // Create dataset of 100 elems before each
    spark.createDataset(1 to 100).write.parquet(inDir)
  }

  after {
    // Cleanup
    FileUtils.deleteDirectory(new File(tmpDir))
  }


  "Job1" should "read parquet from inPath, limit to size, and write to outPath in parquet" taggedAs Slow in {
    Job1(inDir, outDir, 2).run()

    val results = spark.read.parquet(outDir).collect()
    results should contain only (
      Row(1),
      Row(2)
    )
  }

}
