package com.sbchapin.scala

import com.sbchapin.scala.args.{Job1Arguments, Job2Arguments, MainArguments, MalformedCommandException}
import com.sbchapin.scala.config.{MainConfiguration, TransformJobConfiguration}
import com.sbchapin.scala.jobs.{Job, Job1, Job2, JobSupplier}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Inside, Matchers}

/**
  * Tests for the main controller.
  *
  * Examples of...
  * - `mock` for creating mocked objects, wiring them, and expecting function calls from them
  * - `inside` for inspecting & testing complex objects
  * - Exception testing
  *
  * @author sbchapin
  * @since 7/3/18.
  */
class MainControllerSpec extends FlatSpec with Matchers with Inside with MockFactory {

  lazy val job1Args = Job1Arguments(42)
  lazy val job2Args = Job2Arguments(Seq("really", "neat"))
  lazy val mainConf = MainConfiguration(job1 = job1Conf, job2 = job2Conf)
  lazy val job1Conf = TransformJobConfiguration("in1", "out1")
  lazy val job2Conf = TransformJobConfiguration("in2", "out2")

  "A MainController" should "preconfigure a Job1 when passed Job1Arguments" in {
    val controller = new MainController(
      arguments =     MainArguments(jobArgs = Some(job1Args)),
      configuration = mainConf
    )
    val job: Job = controller.job1Supplier.job
    inside(job) { case Job1(actualIn, actualOut, actualSize) =>
      actualIn shouldBe "in1"
      actualOut shouldBe "out1"
      actualSize shouldBe 42
    }
  }

  it should "preconfigure a Job2 when passed Job2Arguments" in {
    val controller = new MainController(
      arguments =     MainArguments(jobArgs = Some(job2Args)),
      configuration = mainConf
    )
    val job: Job = controller.job2Supplier.job
    inside(job) { case Job2(actualIn, actualOut, actualCoolWords) =>
      actualIn shouldBe "in2"
      actualOut shouldBe "out2"
      actualCoolWords shouldBe Seq("really", "neat")
    }
  }

  it should "use the preconfigured supplier when started with Job1Arguments" in {

    // Setup mock
    val mockSupplier = mock[JobSupplier]
    val mockJob = mock[Job]
    (mockSupplier.job _).expects().once().returning(mockJob)

    // Setup controller, overriding supplier with mock
    val controller = new MainController(
      arguments =     MainArguments(jobArgs = Some(job1Args)),
      configuration = mainConf
    ) {
      override lazy val job1Supplier: JobSupplier = mockSupplier
    }

    // The job provided from the supplier should be asked to run
    (mockJob.run _).expects().once()
    controller.start()
  }

  it should "use the preconfigured supplier when started with Job2Arguments" in {

    // Setup mock
    val mockSupplier = mock[JobSupplier]
    val mockJob = mock[Job]
    (mockSupplier.job _).expects().once().returning(mockJob)

    // Setup controller, overriding supplier with mock
    val controller = new MainController(
      arguments =     MainArguments(jobArgs = Some(job2Args)),
      configuration = mainConf
    ) {
      override lazy val job2Supplier: JobSupplier = mockSupplier
    }

    // The job provided from the supplier should be asked to run
    (mockJob.run _).expects().once()
    controller.start()
  }

  it should "throw exception when started with no job arguments" in {
    val controller = new MainController(
      arguments =     MainArguments(jobArgs = None),
      configuration = mainConf
    )
    a [MalformedCommandException.type] should be thrownBy controller.start()
  }

}