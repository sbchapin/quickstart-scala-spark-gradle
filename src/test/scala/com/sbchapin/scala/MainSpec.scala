package com.sbchapin.scala

import com.sbchapin.scala.args.{Job1Arguments, Job2Arguments}
import com.sbchapin.scala.config.{MainConfiguration, TransformJobConfiguration}
import org.scalatest.{FlatSpec, Inside, Matchers}

/**
  * Tests for the components of the main entrypoint.
  *
  * Examples of...
  * - `inside` for inspecting & testing complex objects
  * - Exception testing
  *
  * @author sbchapin
  * @since 7/3/18.
  */
class MainSpec extends FlatSpec with Matchers with Inside {

  // Test job 1

  "A Main.ArgParser" should "parse job-1 as a command with a default size of 5" in {
    val argParser = new Main.ArgParser
    val args = argParser.parseOrDie(Array("job-1"))
    args.jobArgs.get shouldBe a[Job1Arguments]
    inside(args.jobArgs) { case Some(Job1Arguments(defaultSize)) =>
      defaultSize shouldBe 5 // Assert default
    }
  }

  it should "parse job-1 as a command with input size param" in {
    val argParser = new Main.ArgParser
    val args = argParser.parseOrDie(Array("job-1", "--size", "42"))
    args.jobArgs.get shouldBe a[Job1Arguments]
    inside(args.jobArgs) { case Some(Job1Arguments(parsedSize)) =>
      parsedSize shouldBe 42 // Assert the parsed value
    }
  }

  // Test job 2

  it should "parse job-2 as a command with a default list of words" in {
    val argParser = new Main.ArgParser
    val args = argParser.parseOrDie(Array("job-2"))
    args.jobArgs.get shouldBe a[Job2Arguments]
    inside(args.jobArgs) { case Some(Job2Arguments(defaultCoolWords)) =>
      defaultCoolWords shouldBe Seq("phantasm", "quotidian", "brobdingnagian") // Assert default
    }
  }

  it should "parse job-2 as a command with input cool-words param" in {
    val argParser = new Main.ArgParser
    val args = argParser.parseOrDie(Array("job-2", "--cool-words", "real,neato"))
    args.jobArgs.get shouldBe a[Job2Arguments]
    inside(args.jobArgs) { case Some(Job2Arguments(parsedCoolWords)) =>
      parsedCoolWords shouldBe Seq("real", "neato") // Assert the parsed value
    }
  }

  // Test outer flags

  it should "parse with --prod affecting the configuration file and spark session" in {
    val argParser = new Main.ArgParser
    val args = argParser.parseOrDie(Array("job-1", "--prod"))
    args.isProd shouldBe true
    args.configurationLocation should endWith("prod.conf")
    args.sparkMode shouldBe SparkModes.SparkSubmit
  }

  it should "parse with the absensce of --prod affecting the configuration file and spark session" in {
    val argParser = new Main.ArgParser
    val args = argParser.parseOrDie(Array("job-1"))
    args.isProd shouldBe false
    args.configurationLocation should endWith("test.conf")
    args.sparkMode shouldBe SparkModes.Local
  }

  // Test error states

  it should "throw exception when command not passed" in {
    val argParser = new Main.ArgParser
    an [Exception] should be thrownBy argParser.parseOrDie(Array())
  }

  // Test Configuration parsing alongside arg parsing

  "A MainConfiguration" should "be parsed from test.conf, using Main.ArgParser to load conf" in {
    val argParser = new Main.ArgParser
    val args = argParser.parseOrDie(Array("job-1"))
    args.configurationLocation should endWith("test.conf")
    val conf = pureconfig.loadConfigOrThrow[MainConfiguration](args.conf)
    inside(conf) { case MainConfiguration(TransformJobConfiguration(in1, out1), TransformJobConfiguration(in2, out2)) =>
      in1  shouldBe "data/dirt"
      out1 shouldBe "data/grass"
      in2  shouldBe "data/grass"
      out2 shouldBe "data/animal"
    }
  }

  it should "be parsed from prod.conf, using Main.ArgParser to load conf" in {
    val argParser = new Main.ArgParser
    val args = argParser.parseOrDie(Array("job-1", "--prod"))
    args.configurationLocation should endWith("prod.conf")
    val conf = pureconfig.loadConfigOrThrow[MainConfiguration](args.conf)
    inside(conf) { case MainConfiguration(TransformJobConfiguration(in1, out1), TransformJobConfiguration(in2, out2)) =>
      in1  shouldBe "data/v0.0.0/dirt"
      out1 shouldBe "data/v0.0.0/grass"
      in2  shouldBe "data/v0.0.0/grass"
      out2 shouldBe "data/v0.0.0/animal"
    }
  }


}